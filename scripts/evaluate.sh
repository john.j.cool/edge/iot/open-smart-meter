#!/bin/bash
# test
# base comes form https://github.com/Linux-RISC/Sungrow-Meter-cheater
# many thanks :-)

device="/dev/ttyUSB0"
stty -F $device 9600 -parenb -parodd -cmspar cs8 -hupcl -cstopb cread clocal -crtscts -ignbrk -brkint -ignpar -parmrk -inpck -istrip -inlcr -igncr -icrnl -ixon -ixoff -iuclc -ixany -imaxbel -iutf8 -opost -olcuc -ocrnl onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0 -isig -icanon -iexten -echo echoe echok -echonl -noflsh -xcase -tostop -echoprt echoctl echoke -flusho -extproc

# register 63, 1 registers
R63_1="fe03003f0001a009"
#      FE03003F0001A009
#      FE03500000018105
# register 356, 8 registers
R356_8="fe03016400081020"
#       FE03016400081020
# register 10, 12 registers
R10_12="fe03000a000c71c2"
# register 97, 3 registers
R97_3="fe0300610003401a"
# register 119, 1 registers
R119_1="fe0300770001201f"




while true
do
  request=$(xxd -l 8 -p $device)
  case $request in
    $R63_1)
      echo "answering to $request (register 63, 1 registers)"
      answer="FE03020000AC50"
      echo "$answer" | xxd -r -p > $device
      ;;
    $R356_8)
      echo "answering to $request (register 356, 8 registers)"
      ;;
    $R10_12)
      echo "answering to $request (register 10, 12 registers)"
      ;;
    $R97_3)
      echo "answering to $request (register 97, 3 registers)"
      ;;
    $R119_1)
      echo "answering to $request (register 119, 1 registers)"
      ;;
    *)
      echo "unknown request $request"
      ;;
  esac
done