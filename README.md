# Open Smart Meter

## Usage
Starting with container image ```johnjcool/open-smart-meter:0.0.5``` supported platforms are linux/amd64 and linux/arm64.

1. create application.yaml
    ```yaml
    logging:
      level:
        root: info
        com.ghgande.j2mod.modbus.net: debug
    
    smart-meter:
      mqtt:
        url: tcp://<hostname>:1883
        client-id: open-smart-meter
        topic: tele/<sensor_id>/SENSOR
      modbus:
        type: SERIAL
        serial:
          portName: /dev/ttyUSB0
    ```
2. run container
    ```bash
    docker run --device=/dev/ttyUSB0:/dev/ttyUSB0 -v $(pwd)/application.yaml:/application.yaml johnjcool/open-smart-meter:0.0.5
    ```


## Getting started

- Communication forms RS485
- Communication protocol Modbus-RTU No check bit, 1 stop bit
- Communication Address (ID) 254 Factory default
- Baud rate 9600bps Factory default
- function code 03H、10H
- 03H：Read registers (R and R/W)；
- 10H：Set a set of multiple registers
- Device type coding 0x20D5
- Add the value 0x20D5 (hexadecimal)
- to the address of the 0x5000 register,
- which is used for our equipment to
- identify the electricity meter. This
- register is read-only

### Modbus request
- 11	  slave id (17 = 11 HEX)
- 03	  function code
- 006B  adress of first register (40108-40001 = 107 = 6B hex)
- 0003  count of registers to read (read 3 register from 40108 to 40110)
- 7687  CRC checksum

### Example Sungrow SG12RT ```FE03003F0001A009``:
- fe    slave id - dec 254 --> factory default
- 03    function code - dec 3 --> only read
- 003f  register - dec 63 --> ???
- 0001  read 1 register - dec 1 
- a009  CRC checksum - dec 40969

FE 03 5000 0001 8105

### Modbus response:
- 11	slave id (17 = 11 hex)
- 03	function code
- 06	count of response bytes (6 bytes)
- AE	high register bytes (AE hex)	register value Hi (AO0)
- 41	low register bytes (41 hex)	    register value Lo (AO0)
- 56	high register bytes (56 hex)	register value Hi (AO1)
- 52	low register bytes (52 hex)	    register value Lo (AO1)
- 43	high register bytes (43 hex)	register value Hi (AO2)
- 40	low register bytes (40 hex)	    register value Lo (AO2)
- 49	CRC checksum Hi
- AD	CRC checksum Lo

### Example Sungrow SG12RT ```FE03020000AC50```:
- FE slave id  - dec 254
- 03 function code - dec 3
- 02 count of response bytes (2 bytes)
- 00 high register bytes dec 0 register value Hi (AO0)
- 00 low register bytes dec 0 register value Lo (AO0)
- AC CRC checksum Hi 172
- 50 CRC checksum Lo 80


## Setup
see https://github.com/Linux-RISC/Sungrow-Meter-cheater#configuring-homepi

## Useful links
- https://github.com/Linux-RISC/Sungrow-Meter-cheater
- https://github.com/octera/energy-center/

catch FE 83 02 --> not found


```bash
docker run -v $(pwd)/src/test/resources:/app/examples helloysd/modpoll modpoll --tcp 192.168.2.141 --print-result --loglevel DEBUG --config /app/examples/meter.csv

docker run -v $(pwd)/src/test/resources:/app/examples helloysd/modpoll modpoll --tcp 192.168.2.171 --print-result --loglevel DEBUG --config /app/examples/inverter.csv




```