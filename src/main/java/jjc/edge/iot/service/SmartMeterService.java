package jjc.edge.iot.service;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.slave.ModbusSlave;
import jakarta.annotation.PreDestroy;
import jjc.edge.iot.model.SmartMeterLanguageMessage;
import jjc.edge.iot.model.SungrowSmartMeter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SmartMeterService {

    private final ModbusSlave modbusSlave;
    private final SungrowSmartMeter smartMeter;

    @Autowired
    public SmartMeterService(final ModbusSlave modbusSlave, final SungrowSmartMeter smartMeter) {
        this.modbusSlave = modbusSlave;
        this.smartMeter = smartMeter;
        this.modbusSlave.addProcessImage(this.smartMeter.getUnitID(), this.smartMeter);
    }

    public void handleMessage(SmartMeterLanguageMessage message) throws MessagingException, ModbusException {
        LOGGER.info("{}", message);
        var payload = message.getPayload();
        this.smartMeter.setTotalForwardActiveEnergy(Math.round(payload.getTotalKwh() * 100));
        this.smartMeter.setTotalReverseActiveEnergy(Math.round(payload.getExportTotalKwh() * 100));

        this.smartMeter.setVoltageA(Math.round(payload.getVoltP1()));
        this.smartMeter.setVoltageB(Math.round(payload.getVoltP2()));
        this.smartMeter.setVoltageC(Math.round(payload.getVoltP3()));

        this.smartMeter.setFrequency(payload.getFreq() * 10);

        this.smartMeter.setActivePowerA(Math.round(payload.getVoltP1() * payload.getAmpP1()));
        this.smartMeter.setActivePowerB(Math.round(payload.getVoltP2() * payload.getAmpP2()));
        this.smartMeter.setActivePowerC(Math.round(payload.getVoltP3() * payload.getAmpP3()));

        this.smartMeter.setTotalActivePower(Math.round(payload.getPowerCurr()));
        this.modbusSlave.open();
    }

    @PreDestroy
    protected void destroy() {
        modbusSlave.close();
    }
}