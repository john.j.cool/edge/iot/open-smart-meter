package jjc.edge.iot.utils;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.io.PrintStream;

/**
 * This is {@link AsciiArtUtils}.
 */
@UtilityClass
public class AsciiArtUtils {
    /**
     * Print ascii art.
     *
     * @param out        the out
     * @param asciiArt   the ascii art
     * @param additional the additional
     */
    @SneakyThrows
    public static void printAsciiArt(final PrintStream out, final String asciiArt, final String additional) {
        if (StringUtils.isNotBlank(additional)) {
            out.println(asciiArt);
            out.println(additional);
        } else {
            out.print(asciiArt);
        }
    }

    /**
     * Print ascii art.
     *
     * @param out        the out
     * @param additional the additional
     */
    @SneakyThrows
    public static void printAsciiArtWarning(final Logger out, final String additional) {
        var ascii = "\n"
                + "  ____ _____ ___  ____  _ \n"
                + " / ___|_   _/ _ \\|  _ \\| |\n"
                + " \\___ \\ | || | | | |_) | |\n"
                + "  ___) || || |_| |  __/|_|\n"
                + " |____/ |_| \\___/|_|   (_)\n"
                + "                          \n";
        out.warn("\n\n".concat(ascii).concat(additional));
    }

    /**
     * Print ascii art info.
     *
     * @param out        the out
     * @param additional the additional
     */
    @SneakyThrows
    public static void printAsciiArtReady(final Logger out, final String additional) {
        var ascii = "\n"
                + "  ____  _____    _    ______   __\n"
                + " |  _ \\| ____|  / \\  |  _ \\ \\ / /\n"
                + " | |_) |  _|   / _ \\ | | | \\ V / \n"
                + " |  _ <| |___ / ___ \\| |_| || |  \n"
                + " |_| \\_\\_____/_/   \\_\\____/ |_|  \n"
                + "                                 \n";
        out.info("\n\n".concat(ascii).concat(additional));
    }
}
