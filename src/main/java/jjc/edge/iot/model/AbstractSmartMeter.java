package jjc.edge.iot.model;

import com.ghgande.j2mod.modbus.procimg.SimpleProcessImage;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;

public abstract class AbstractSmartMeter extends SimpleProcessImage {
    public AbstractSmartMeter(int unit) {
        super(unit);
    }

    public SimpleRegister[] toRegisters(final int value) {
        var uint32 = Integer.toUnsignedLong(value);
        var registers = new byte[4];
        registers[0] = (byte) (0xff & (uint32 >> 24));
        registers[1] = (byte) (0xff & (uint32 >> 16));
        registers[2] = (byte) (0xff & (uint32 >> 8));
        registers[3] = (byte) (0xff & uint32);
        return new SimpleRegister[]{
                new SimpleRegister(registers[0], registers[1]),
                new SimpleRegister(registers[2], registers[3])
        };
    }

    abstract void setTotalForwardActiveEnergy(final int value);

    abstract void setTotalReverseActiveEnergy(final int value);

    abstract void setActivePowerA(final int value);

    abstract void setActivePowerB(final int value);

    abstract void setActivePowerC(final int value);

    abstract void setTotalActivePower(final int value);

    abstract void setVoltageA(final int value);

    abstract void setVoltageB(final int value);

    abstract void setVoltageC(final int value);

    abstract void setFrequency(final int value);
}
