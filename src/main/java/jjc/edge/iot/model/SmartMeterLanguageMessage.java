package jjc.edge.iot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class SmartMeterLanguageMessage {

    @JsonProperty("Time")
    private Date time;

    @JsonProperty("SML")
    private Payload payload;

    @Data
    @Builder
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Payload {
        @JsonProperty("server_id")
        private String serverId;
        @JsonProperty("total_kwh")
        private float totalKwh;
        @JsonProperty("export_total_kwh")
        private float exportTotalKwh;
        @JsonProperty("Power_curr")
        private float powerCurr;
        @JsonProperty("volt_p1")
        private float voltP1;
        @JsonProperty("volt_p2")
        private float voltP2;
        @JsonProperty("volt_p3")
        private float voltP3;
        @JsonProperty("amp_p1")
        private float ampP1;
        @JsonProperty("amp_p2")
        private float ampP2;
        @JsonProperty("amp_p3")
        private float ampP3;
        @JsonProperty("phase_angle_l2_l1")
        private float phaseAngleL2L1;
        @JsonProperty("phase_angle_l3_l1")
        private float phaseAngleL3L1;
        @JsonProperty("phase_angle_p1")
        private float phaseAngleP1;
        @JsonProperty("phase_angle_p2")
        private float phaseAngleP2;
        @JsonProperty("phase_angle_p3")
        private float phaseAngleP3;
        @JsonProperty("freq")
        private int freq;
    }
}
