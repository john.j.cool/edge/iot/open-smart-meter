package jjc.edge.iot.model;

import com.ghgande.j2mod.modbus.procimg.SimpleRegister;

public class SungrowSmartMeter extends AbstractSmartMeter {

    public SungrowSmartMeter() {
        // slave id
        super(254);

        // Current total forward active energy (Aktuelle Gesamtvorwärtswirkenergie) - length 4
        addRegister(10, new SimpleRegister(0));
        addRegister(11, new SimpleRegister(0));
        // Current forward active peak electric energy (Aktuelle aktive elektrische Spitzenenergie) - length 4
        addRegister(12, new SimpleRegister(0));
        addRegister(13, new SimpleRegister(0));
        // Current forward active peak electric energy (Aktuelle aktive elektrische Spitzenenergie) - length 4
        addRegister(14, new SimpleRegister(0));
        addRegister(15, new SimpleRegister(0));
        // Current forward active power level (Aktueller Vorwärtswirkleistungspegel) - length 4
        addRegister(16, new SimpleRegister(0));
        addRegister(17, new SimpleRegister(0));
        // Current forward active valley power (Aktuelle aktive Vorwärtstalleistung) - length 4
        addRegister(18, new SimpleRegister(0));
        addRegister(19, new SimpleRegister(0));
        // Current total reverse active energy (Aktuelle gesamte aktive Rückwärtsenergie) - length 4
        addRegister(20, new SimpleRegister(0));
        addRegister(21, new SimpleRegister(0));

        // First communication path:Address - length 2 --> rw
        addRegister(63, new SimpleRegister(0));

        // Voltage of A phase - length 2
        addRegister(97, new SimpleRegister(0));
        // Voltage of B phase - length 2
        addRegister(98, new SimpleRegister(0));
        // Voltage of C phase - length 2
        addRegister(99, new SimpleRegister(0));

        // Frequency - length 2
        addRegister(119, new SimpleRegister(0));

        // Active power of A phase - length 4
        addRegister(356, new SimpleRegister(0));
        addRegister(357, new SimpleRegister(0));

        // Active power of B phase - length 4
        addRegister(358, new SimpleRegister(0));
        addRegister(359, new SimpleRegister(0));

        // Active power of C phase - length 4
        addRegister(360, new SimpleRegister(0));
        addRegister(361, new SimpleRegister(0));

        // Total active power - length 4
        addRegister(362, new SimpleRegister(0));
        addRegister(363, new SimpleRegister(0));

        // Device type coding - Sungrow flagged DTSU 666 meter
        addRegister(20480, new SimpleRegister(8405));
    }

    public void setTotalForwardActiveEnergy(final int value) {
        var registers = toRegisters(value);
        setRegister(10, registers[0]);
        setRegister(11, registers[1]);
    }

    public void setTotalReverseActiveEnergy(final int value) {
        var registers = toRegisters(value);
        setRegister(20, registers[0]);
        setRegister(21, registers[1]);
    }

    public void setActivePowerA(final int value) {
        var registers = toRegisters(value);
        setRegister(356, registers[0]);
        setRegister(357, registers[1]);
    }

    public void setActivePowerB(final int value) {
        var registers = toRegisters(value);
        setRegister(358, registers[0]);
        setRegister(359, registers[1]);
    }

    public void setActivePowerC(final int value) {
        var registers = toRegisters(value);
        setRegister(360, registers[0]);
        setRegister(361, registers[1]);
    }

    public void setTotalActivePower(final int value) {
        var registers = toRegisters(value);
        setRegister(362, registers[0]);
        setRegister(363, registers[1]);
    }

    public void setVoltageA(final int value) {
        setRegister(97, new SimpleRegister(value));
    }

    public void setVoltageB(final int value) {
        setRegister(98, new SimpleRegister(value));
    }

    public void setVoltageC(final int value) {
        setRegister(99, new SimpleRegister(value));
    }

    public void setFrequency(final int value) {
        setRegister(119, new SimpleRegister(value));
    }
}
