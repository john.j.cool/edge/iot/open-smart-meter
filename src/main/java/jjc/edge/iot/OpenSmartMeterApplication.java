package jjc.edge.iot;

import jjc.edge.iot.config.properties.SmartMeterProperties;
import jjc.edge.iot.utils.AsciiArtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.event.EventListener;
import reactor.core.publisher.Hooks;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties({SmartMeterProperties.class})
public class OpenSmartMeterApplication {

    public static void main(String[] args) {
        Hooks.enableAutomaticContextPropagation();
        new SpringApplicationBuilder(OpenSmartMeterApplication.class)
                .banner(new OpenSmartMeterBanner())
                .run(args);
    }

    /**
     * Handle application ready event.
     *
     * @param event the event
     */
    @EventListener
    public void handleApplicationReadyEvent(final ApplicationReadyEvent event) {
        AsciiArtUtils.printAsciiArtReady(LOGGER, StringUtils.EMPTY);
        LOGGER.info(
                "Ready to process requests @ [{}]",
                ZonedDateTime.ofInstant(
                        Instant.ofEpochMilli(event.getTimestamp()),
                        ZoneOffset.UTC
                )
        );
    }
}
