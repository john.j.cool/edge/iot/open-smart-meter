package jjc.edge.iot;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringBootVersion;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.core.SpringVersion;
import org.springframework.core.env.Environment;

import javax.crypto.Cipher;
import java.io.PrintStream;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * This is {@link OpenSmartMeterBanner}.
 */
public class OpenSmartMeterBanner implements Banner {

    // http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=MO%20WebSSH%20Auth
    private static final String[] BANNER = {
            " ██████╗ ██████╗ ███████╗███╗   ██╗    ███████╗███╗   ███╗ █████╗ ██████╗ ████████╗    ███╗   ███╗███████╗████████╗███████╗██████╗ ",
            "██╔═══██╗██╔══██╗██╔════╝████╗  ██║    ██╔════╝████╗ ████║██╔══██╗██╔══██╗╚══██╔══╝    ████╗ ████║██╔════╝╚══██╔══╝██╔════╝██╔══██╗",
            "██║   ██║██████╔╝█████╗  ██╔██╗ ██║    ███████╗██╔████╔██║███████║██████╔╝   ██║       ██╔████╔██║█████╗     ██║   █████╗  ██████╔╝",
            "██║   ██║██╔═══╝ ██╔══╝  ██║╚██╗██║    ╚════██║██║╚██╔╝██║██╔══██║██╔══██╗   ██║       ██║╚██╔╝██║██╔══╝     ██║   ██╔══╝  ██╔══██╗",
            "╚██████╔╝██║     ███████╗██║ ╚████║    ███████║██║ ╚═╝ ██║██║  ██║██║  ██║   ██║       ██║ ╚═╝ ██║███████╗   ██║   ███████╗██║  ██║",
            " ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═══╝    ╚══════╝╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝       ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚══════╝╚═╝  ╚═╝",
    };

    private static final String[] BANNER_BY = {
            " ________      ___    ___         ___  ________  ___  ___  ________             ___      ________  ________  ________  ___",
            "|\\   __  \\    |\\  \\  /  /|       |\\  \\|\\   __  \\|\\  \\|\\  \\|\\   ___  \\          |\\  \\    |\\   ____\\|\\   __  \\|\\   __  \\|\\  \\",
            "\\ \\  \\|\\ /_   \\ \\  \\/  / /       \\ \\  \\ \\  \\|\\  \\ \\  \\\\\\  \\ \\  \\\\ \\  \\         \\ \\  \\   \\ \\  \\___|\\ \\  \\|\\  \\ \\  \\|\\  \\ \\  \\",
            " \\ \\   __  \\   \\ \\    / /      __ \\ \\  \\ \\  \\\\\\  \\ \\   __  \\ \\  \\\\ \\  \\      __ \\ \\  \\   \\ \\  \\    \\ \\  \\\\\\  \\ \\  \\\\\\  \\ \\  \\",
            "  \\ \\  \\|\\  \\   \\/  /  /      |\\  \\\\_\\  \\ \\  \\\\\\  \\ \\  \\ \\  \\ \\  \\\\ \\  \\ ___|\\  \\\\_\\  \\ __\\ \\  \\____\\ \\  \\\\\\  \\ \\  \\\\\\  \\ \\  \\____",
            "   \\ \\_______\\__/  / /        \\ \\________\\ \\_______\\ \\__\\ \\__\\ \\__\\\\ \\__\\\\__\\ \\________\\\\__\\ \\_______\\ \\_______\\ \\_______\\ \\_______\\",
            "    \\|_______|\\___/ /          \\|________|\\|_______|\\|__|\\|__|\\|__| \\|__\\|__|\\|________\\|__|\\|_______|\\|_______|\\|_______|\\|_______|",
            "             \\|___|/"
    };

    private static final int SEPARATOR_REPEAT_COUNT = 60;
    private static final String SEPARATOR_CHAR = "-";

    /**
     * Line separator string.
     */
    protected static final String LINE_SEPARATOR = String.join(StringUtils.EMPTY, Collections.nCopies(SEPARATOR_REPEAT_COUNT, SEPARATOR_CHAR));

    private static boolean isJceInstalled() {
        try {
            int maxKeyLen = Cipher.getMaxAllowedKeyLength("AES");
            return maxKeyLen == 2147483647;
        } catch (NoSuchAlgorithmException var1) {
            return false;
        }
    }

    @Override
    public void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {
        for (String line : BANNER) {
            out.println(AnsiOutput.toString(AnsiColor.BRIGHT_BLUE, line));
        }
        for (String line : BANNER_BY) {
            out.println(AnsiOutput.toString(AnsiColor.BRIGHT_GREEN, line));
        }
        out.println(collectEnvironmentInfo(environment, sourceClass));
        out.println();
    }

    /**
     * Collect environment info with
     * details on the java and os deployment
     * versions.
     *
     * @param environment the environment
     * @param sourceClass the source class
     * @return environment info
     */
    private String collectEnvironmentInfo(final Environment environment, final Class<?> sourceClass) {
        try (var formatter = new Formatter()) {
            var sysInfo = new LinkedHashMap<String, Object>();
            sysInfo.put("App Version", Objects.toString(getApplicationVersion(sourceClass), "Not Available"));
            sysInfo.put("Spring Boot Version", SpringBootVersion.getVersion());
            sysInfo.put("Spring Version", SpringVersion.getVersion());
            Properties properties = System.getProperties();
            sysInfo.put("Java Home", properties.get("java.home"));
            sysInfo.put("Java Vendor", properties.get("java.vendor"));
            sysInfo.put("Java Version", properties.get("java.version"));
            Runtime runtime = Runtime.getRuntime();
            sysInfo.put("JVM Free Memory", FileUtils.byteCountToDisplaySize(runtime.freeMemory()));
            sysInfo.put("JVM Maximum Memory", FileUtils.byteCountToDisplaySize(runtime.maxMemory()));
            sysInfo.put("JVM Total Memory", FileUtils.byteCountToDisplaySize(runtime.totalMemory()));
            sysInfo.put("JCE Installed", StringUtils.capitalize(BooleanUtils.toStringYesNo(isJceInstalled())));
            sysInfo.put("OS Architecture", properties.get("os.arch"));
            sysInfo.put("OS Name", properties.get("os.name"));
            sysInfo.put("OS Version", properties.get("os.version"));
            sysInfo.put("OS Date/Time", LocalDateTime.now());
            sysInfo.put("OS Temp Directory", FileUtils.getTempDirectoryPath());
            sysInfo.forEach((k, v) -> {
                if (k.startsWith(SEPARATOR_CHAR)) {
                    formatter.format("%s%n", LINE_SEPARATOR);
                } else {
                    formatter.format("%s: %s%n", k, v);
                }
            });
            formatter.format("%s%n", LINE_SEPARATOR);
            injectEnvironmentInfoIntoBanner(formatter, environment);
            return formatter.toString();
        }
    }

    protected void injectEnvironmentInfoIntoBanner(final Formatter formatter, final Environment environment) {
        if (ArrayUtils.isEmpty(environment.getActiveProfiles())) {
            formatter.format("Environment: %s%n", Arrays.toString(environment.getDefaultProfiles()));
        } else {
            formatter.format("Environment: %s%n", Arrays.toString(environment.getActiveProfiles()));
        }
        formatter.format("%s%n", LINE_SEPARATOR);
    }

    protected String getApplicationVersion(Class<?> sourceClass) {
        Package sourcePackage = (sourceClass != null) ? sourceClass.getPackage() : null;
        return (sourcePackage != null) ? sourcePackage.getImplementationVersion() : null;
    }
}
