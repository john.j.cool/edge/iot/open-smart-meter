package jjc.edge.iot.config;

import jjc.edge.iot.config.properties.SmartMeterProperties;
import jjc.edge.iot.model.SmartMeterLanguageMessage;
import jjc.edge.iot.service.SmartMeterService;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.Transformers;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;

@Configuration
public class MqttConfiguration {

    @Bean
    public IntegrationFlow mqttInbound(final SmartMeterService smartMeterService, SmartMeterProperties properties) {
        return IntegrationFlow.from(
                        new MqttPahoMessageDrivenChannelAdapter(
                                properties.getMqtt().getUrl(),
                                properties.getMqtt().getClientId(),
                                mqttPahoClientFactory(),
                                properties.getMqtt().getTopic()
                        )
                )
                .transform(Transformers.fromJson(SmartMeterLanguageMessage.class))
                .handle(smartMeterService, "handleMessage")
                .get();
    }

    private MqttPahoClientFactory mqttPahoClientFactory(){
        var factory = new DefaultMqttPahoClientFactory();
        var options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        factory.setConnectionOptions(options);
        return factory;
    }
}
