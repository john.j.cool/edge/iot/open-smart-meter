package jjc.edge.iot.config.properties;

import com.ghgande.j2mod.modbus.slave.ModbusSlaveType;
import com.ghgande.j2mod.modbus.util.SerialParameters;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "smart-meter")
public class SmartMeterProperties {

    private MqttProperties mqtt;

    private ModbusProperties modbus;

    @Data
    public static class MqttProperties {
        private String url;
        private String clientId;
        private String topic;
        private String username;
        private String password;
    }

    @Data
    public static class ModbusProperties {
        private ModbusSlaveType type;
        private TcpParameters tcp = new TcpParameters();
        private SerialParameters serial = new SerialParameters();
    }

    @Data
    public static class TcpParameters {
        private int port = 502;
        private int poolSize = 10;
        private boolean useRtuOverTcp;
    }
}
