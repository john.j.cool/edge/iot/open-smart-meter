package jjc.edge.iot.config;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class MicrometerConfiguration {

    @Value("${spring.application.name}")
    private String application;

    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags(final Environment environment) {
        var profile = "default";
        if (environment.getActiveProfiles().length > 0) {
            profile = environment.getActiveProfiles()[0];
        }
        var activeProfile = profile;
        return registry -> registry
                .config()
                .commonTags(
                        "application",
                        application,
                        "active_profile",
                        activeProfile
                );
    }
}
