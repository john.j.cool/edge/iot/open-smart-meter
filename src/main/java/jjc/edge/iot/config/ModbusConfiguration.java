package jjc.edge.iot.config;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.slave.ModbusSlave;
import com.ghgande.j2mod.modbus.slave.ModbusSlaveFactory;
import jjc.edge.iot.config.properties.SmartMeterProperties;
import jjc.edge.iot.model.SungrowSmartMeter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModbusConfiguration {

    @Bean
    ModbusSlave modbusSlave(final SmartMeterProperties properties) throws ModbusException {
        return switch (properties.getModbus().getType()) {
            case TCP -> ModbusSlaveFactory.createTCPSlave(
                    properties.getModbus().getTcp().getPort(),
                    properties.getModbus().getTcp().getPoolSize(),
                    properties.getModbus().getTcp().isUseRtuOverTcp()
            );
            case SERIAL -> ModbusSlaveFactory.createSerialSlave(properties.getModbus().getSerial());
            case UDP -> throw new UnsupportedOperationException("This type of modbus slave isn't supported");
        };
    }

    @Bean
    SungrowSmartMeter smartMeter() {
        return new SungrowSmartMeter();
    }
}


