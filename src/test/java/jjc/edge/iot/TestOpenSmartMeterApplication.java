package jjc.edge.iot;

import jjc.edge.iot.config.properties.SmartMeterProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import reactor.core.publisher.Hooks;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties({SmartMeterProperties.class})
public class TestOpenSmartMeterApplication {

    public static void main(String[] args) {
        Hooks.enableAutomaticContextPropagation();
        new SpringApplicationBuilder(OpenSmartMeterApplication.class)
                .banner(new OpenSmartMeterBanner())
                .run(args);
    }
}
