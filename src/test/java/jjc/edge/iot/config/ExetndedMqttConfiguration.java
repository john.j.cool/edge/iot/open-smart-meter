package jjc.edge.iot.config;

import jjc.edge.iot.config.properties.SmartMeterProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
public class ExetndedMqttConfiguration {

    private static final String CLIENT_ID = "tasmota-simulator";

    @Bean
    public IntegrationFlow mqttOutbound(final SmartMeterProperties properties) {
        var mqttHandler = new MqttPahoMessageHandler(
                properties.getMqtt().getUrl(),
                CLIENT_ID
        );
        mqttHandler.setDefaultTopic(properties.getMqtt().getTopic());
        return f -> f.handle(mqttHandler);
    }
}
