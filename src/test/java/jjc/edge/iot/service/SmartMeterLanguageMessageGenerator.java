package jjc.edge.iot.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jjc.edge.iot.model.SmartMeterLanguageMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Random;

@Slf4j
@Service
public class SmartMeterLanguageMessageGenerator {

    private final IntegrationFlow mqttOutbound;

    private final ObjectMapper objectMapper;

    private float exportTotalKwh = 0;

    @Autowired
    public SmartMeterLanguageMessageGenerator(final IntegrationFlow mqttOutbound, final Jackson2ObjectMapperBuilder builder) {
        this.mqttOutbound = mqttOutbound;
        objectMapper = builder.build();
    }

    @Scheduled(fixedDelay = 10000)
    public void scheduleFixedDelayTask() throws JsonProcessingException {
        mqttOutbound.getInputChannel()
                .send(new GenericMessage<>(objectMapper.writeValueAsString(newSmlMessage())));
    }

    private SmartMeterLanguageMessage newSmlMessage() {
        this.exportTotalKwh = this.exportTotalKwh + 0.02f;
        return SmartMeterLanguageMessage.builder()
                .time(new Date())
                .payload(
                        SmartMeterLanguageMessage.Payload.builder()
                                .serverId("0a01484c590300036074")
                                .totalKwh(3379.0072f)
                                .exportTotalKwh(exportTotalKwh)
                                .powerCurr(powerCurrN())
                                .voltP1(236.0f)
                                .voltP2(238.0f)
                                .voltP3(237.0f)
                                .ampP1(6.0f)
                                .ampP2(6.5f)
                                .ampP3(7.0f)
                                .phaseAngleL2L1(119.0f)
                                .phaseAngleL3L1(239.0f)
                                .phaseAngleP1(180.0f)
                                .phaseAngleP2(182.0f)
                                .phaseAngleP3(192.0f)
                                .freq(50)
                                .build()
                )
                .build();
    }

    public static float powerCurrN() {
        var rnd = new Random();

        var value = rnd.nextDouble(-10000, -100);
        int scale = (int) Math.pow(10, 4);
        return (float) (Math.round(value * scale) / scale);
    }

    public static float powerCurrP() {
        var rnd = new Random();
        var value = rnd.nextDouble(100, 4000);
        int scale = (int) Math.pow(10, 4);
        return (float) (Math.round(value * scale) / scale);
    }
}
